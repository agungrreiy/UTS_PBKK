<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProsesController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|

*/

Route::get('/', function () {
    return view('create021190054');
});

Route::get('/create', [ProsesController::class, 'create']);
Route::get('/proses', [ProsesController::class, 'proses']);



