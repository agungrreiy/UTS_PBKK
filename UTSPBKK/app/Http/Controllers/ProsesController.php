<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProsesController extends Controller
{
    function create(){
        return view('create021190054');
    }

    function proses(Request $request){
        $data = array();
        $data ['npm'] = $request->npm;
        $data ['nama'] = $request->nama;

        return view('view021190054', ['data' => $data]);

    }
    
}
